/**
 * RockCMS Logger
 */

const winston = require("winston");
const level = process.env.LOG_LEVEL || 'debug';
var logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      level: level,
      timestamp: function () {
        return (new Date()).toISOString();
      }
    })
]
});

module.exports = logger
