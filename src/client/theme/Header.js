import React from 'react';

export const TopHeader = (props) => {

  function handleClick(event) {
    //let data = JSON.parse(props.props.data);
    localStorage.setItem(props.props.key, null);
    window.location = '/login';
  }

  return (
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/service-worker-html/" >
        <img src = "./assets/nbcnews.png" alt = ""/> &nbsp; RockCMS - Lite
      </a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search" />
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" onClick={(event) => handleClick(event)}>Sign out</a>
        </li>
      </ul>
    </nav>
  )
};
