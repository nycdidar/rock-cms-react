import React from 'react';

export const TopBar = (props) => {
  return (
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Article</h1>

    <div class="alert alert-primary top-header-notification" role="alert" id = "top-header-notification">
      Hello !! I can run directly from Browser.
    </div>

    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="btn-group" role="group" aria-label="Basic example">
        <button type="button" class="btn btn-primary">Save</button>
        <button type="submit" class="btn btn-secondary btn_publish">Publish</button>
        <button type="button" class="btn btn-success">Cancel</button>
        <button type="button" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
  )
};
