import React from 'react';

export const Sidebar = (props) => {
  return (
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link active link-dashboard" >
            <span data-feather="home"></span>
            Dashboard <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/service-worker-html/article/sample-custom-route-demo">
            <span data-feather="file"></span>
            Article
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/service-worker-html/product/sample-custom-route-demo">
            <span data-feather="shopping-cart"></span>
            Products
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" >
            <span data-feather="users"></span>
            Byline
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" >
            <span data-feather="bar-chart-2"></span>
            Recipe
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" >
            <span data-feather="layers"></span>
            Slideshow
          </a>
        </li>
      </ul>

      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Administration</span>
        <a class="d-flex align-items-center text-muted" >
          <span data-feather="plus-circle"></span>
        </a>
      </h6>
      <ul class="nav flex-column mb-2">
        <li class="nav-item">
          <a class="nav-link link-push-notification" >
            <span data-feather="file-text"></span>
            Push Notification
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/service-worker-html/sample-404">
            <span data-feather="file-text"></span>
            Sample 404 Page
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <span data-feather="file-text"></span>
            About This DEMO
          </a>
        </li>              
      </ul>

      <ul class="list-group">
        <li class="list-group-item list-group-item-warning"><strong>My Recent Contents</strong></li>
        <li class="list-group-item list-group-item-primary"><a >Trump and Golf</a></li>
        <li class="list-group-item list-group-item-secondary"><a >Midterm election</a></li>
        <li class="list-group-item list-group-item-primary"><a >Where is Clinton Now?</a></li>
        <li class="list-group-item list-group-item-secondary"><a >Jurasic Park !!</a></li>
      </ul>

    </div>
  </nav>
  )
};