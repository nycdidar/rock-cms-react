import React from 'react';
import ReactDOM from 'react-dom';

import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import store from "./store/index";
import { authSuccess } from "./actions/index";
import { addArticle } from "./actions/index";

import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import indexRoutes from "./routes/index.jsx";
import registerServiceWorker from './registerServiceWorker';

const hist = createBrowserHistory();

const sessionKey = process.env.ROCK_APP_SESSION_KEY || 'rockcms';
let rockData = { 
  key: sessionKey,
  data: localStorage.getItem(sessionKey)
}

// Make Redux Store available in console.
window.store = store;
window.addArticle = addArticle;
window.authSuccess = authSuccess;

// Run this in console to TEST
// store.dispatch( authSuccess({ userName: 'Rsfsad sadfs fsadfs', id: 1233 }) )
console.log('Store Data: ', store.getState());
store.subscribe(() => { 
  alert("Hello");
  console.log('Redux Response', store.getState()) 
})

console.log('Session Data', rockData);
ReactDOM.render(
  <Provider store = {store}>
    <Router history = {hist}>
      <Switch>
        {indexRoutes.map((prop, key) => {
          if (prop.redirect)
            return <Redirect from = {prop.path} to = {prop.to} key = {key} />;
          return <Route path = {prop.path} render = {(props) => (<prop.component {...props} data = {rockData}/>)}  key = {key} />;
        })}
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
