export const authSuccess = userData => ({ type: "AUTH_SUCCESS", payload: userData });
export const addArticle = article => ({ type: "ADD_ARTICLE", payload: article });