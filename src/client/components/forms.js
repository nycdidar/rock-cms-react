import React from 'react';

const FRM_REQUIRED_CLASS = 'form-required';
const FRM_REQUIRED_LABEL = 'This field is required.';
const FRM_HELP_BLOCK_CLASS = 'help-block with-errors';
const FRM_INPUT_PARENT_CLASS = 'form-group';
const FRM_INPUT_CONTAINER_CLASS = 'form-control';

const slugify = (text) => {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

export const Messages = (props) => {
  return (
    <div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Content Saved Successfully !!.</div>
  )
};

export const InputBox = (props) => {
  const field_name = (props.name) ? props.name : slugify(props.label);
  const field_id = (props.id) ? props.id : slugify(props.name);
  const input_type = (props.type) ? props.type : 'text';
  let validationPattern, dataError, requiredLabel;
  if (props.required === "true") {
    validationPattern = props.validation;
    dataError = props.error;
    requiredLabel = <span class={FRM_REQUIRED_CLASS} title={FRM_REQUIRED_LABEL}>*</span>;
  }
  let print_label;
  if (props.label) {
    print_label = <label>{props.label} {requiredLabel}</label>
  }

  let print_validation;
  if (props.validation) {
    print_validation = <div class = {FRM_HELP_BLOCK_CLASS}></div>;
  }

  let onchangeHandler = (props.onchangeHandler) ? props.onchangeHandler : e => e.target.value;
  console.log('Debug: Input: ',props);
  return (
    <div>
      <div class={FRM_INPUT_PARENT_CLASS}>
        {print_label}
        <input type = {input_type} class = {`${FRM_INPUT_CONTAINER_CLASS} ${props.class}`} pattern = {validationPattern} id = {field_id} name = {field_name} placeholder = {props.placeholder} data-error={dataError} value = {props.value}  onChange = {onchangeHandler} />
        {print_validation}
      </div>
    </div>
  )
};

export const TextBox = (props) => {
  const field_name = (props.name) ? props.name : slugify(props.label);
  let validationPattern, dataError, requiredLabel;
  if (props.required === "true") {
    validationPattern = props.validation;
    dataError = props.error;
    requiredLabel = <span class={FRM_REQUIRED_CLASS} title={FRM_REQUIRED_LABEL}>*</span>;
  }
  return (
    <div>
      <div class={FRM_INPUT_PARENT_CLASS}>
        <label>{props.label} {requiredLabel}</label>
        <textarea class={FRM_INPUT_CONTAINER_CLASS} rows={props.label} name = {field_name} pattern = {validationPattern} placeholder = {props.placeholder} data-error = {dataError} required>{props.value} </textarea>
        <div class={FRM_HELP_BLOCK_CLASS}></div>
      </div>
    </div>
  )
};


export const SelectList = (props) => {
  const field_name = (props.name) ? props.name : slugify(props.label);
  let validationPattern, DataError, requiredLabel;
  if (props.required === "true") {
    validationPattern = props.validation;
    DataError = props.error;
    requiredLabel = <span class={FRM_REQUIRED_CLASS} title={FRM_REQUIRED_LABEL}>*</span>;
  }
  return (
    <div>
      <div class={FRM_INPUT_PARENT_CLASS}>
        <label>{props.label} {requiredLabel}</label>
        <select class={FRM_INPUT_CONTAINER_CLASS} name = {field_name}  data-validate="true" pattern = {validationPattern} data-error={DataError} required >
        <option></option>
        {props.listdata.map((rowData, index) => {
          let selectedVal = (props.selected === rowData.key) ? 'selected' : '';
          return <option value = {rowData.key} selected = {selectedVal}>{rowData.value}</option>;
        })}
        </select>
      </div>
    </div>
  )
};
