const initialState = {
  userdata: {},
  articles: []
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ARTICLE':
      return { ...state, articles: [...state.articles, action.payload] };
    case 'AUTH_SUCCESS':
      return { userdata: action.payload };
    default:
      return state;
  }
};
export default rootReducer;