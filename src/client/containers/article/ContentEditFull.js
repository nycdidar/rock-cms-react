import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './dashboard.css';
import { TopHeader } from '../../theme/Header';
import { Sidebar } from '../../theme/Sidebar';
import { TopBar } from '../../theme/TopBar';
import { Article } from './Article';
import { ArticleSidebar } from './ArticleSidebar';
import List from "../../components/List";

class ContentEditFull extends Component {
  constructor(props){
    super(props);
    console.log('Show PROPS in Content Edit Full', this.props.data);
  }

  render() {
    return (
      <div className="App">
       <TopHeader props = {this.props.data}/>
        <div class="container-fluid">
          <form class="needs-validation" novalidate action ="#">
            <div class="row">
              <Sidebar />
              <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <TopBar />
                <div class="container">
                  <div class="row">
                    <div class="col-xl">
                      <Article />
                    </div>
                    <div class="col-sm-3">
                      <ArticleSidebar />
                      <List />
                    </div>
                  </div>
                </div>
              </main>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default ContentEditFull;