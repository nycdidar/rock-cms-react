import React from 'react';

export const ArticleSidebar = (props) => {
  return (
    <div>
      <div class="form-group">
        <label for="exampleInputPassword1">SEO Headline</label>
        <input type="text" class="form-control form-control-sm" id="seo_headline" placeholder="Enter SEO Info" required />
        <div class="valid-feedback">
          Looks good!
        </div>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Cover Headline</label>
        <input type="text" class="form-control form-control-sm" id="cover_headline" aria-describedby="emailHelp" placeholder="Cover Headline" required />
        <small id="emailHelp" class="form-text text-muted">This will display in main cover.</small>
        <div class="valid-feedback">
          Looks good!
        </div>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Taxonomy</label>
        <select class="form-control form-control-sm">
        <option>Politics</option>
        <option>World News</option>
        <option>Trump</option>
        </select>
      </div>

      <div class="form-group card">
        <img class="card-img-top" src="assets/donald-trump.jpg" alt="" />
        <div class="card-body">
          <p class="card-text">White House plays cleanup on Trump's Sessions tweet, grocery store claim</p>
          <div class="form-group custom-file">
            <input type="file" class="custom-file-input" id="customFile" />
            <label class="custom-file-label" for="customFile">Main Art</label>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="exampleFormControlTextarea1">Summary</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">In a rare briefing with White House reporters on Wednesday — there were just three in July, lasting a total of less than an hour — White House Press Secretary Sarah Sanders tried to tamp down growing   
        </textarea>
      </div>

      <div class="form-group form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" />
        <label class="form-check-label" for="defaultCheck1">
          Shell Page
        </label>
      </div>
      <div class="form-group form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled />
        <label class="form-check-label" for="defaultCheck2">
          Native Ad
        </label>
      </div>
  
      <div class="form-group">
        <label for="exampleInputEmail1">Primary Section</label>
        <select class="form-control form-control-sm">
        <option>About</option>
        <option>Home Page</option>
        <option>Politics</option>
        </select>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Primary Topic</label>
        <select class="form-control form-control-sm">
        <option>Today</option>
        <option>News</option>
        <option>Health</option>
        </select>
      </div>
    </div>
  )
};
