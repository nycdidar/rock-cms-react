import React from 'react';
import tinymce from 'tinymce/js/tinymce/tinymce.min';

import 'tinymce/js/tinymce/skins/lightgray/content.inline.min.css';
import 'tinymce/js/tinymce/skins/lightgray/skin.min.css';
import 'tinymce/js/tinymce/themes/modern/theme.min.js';
import 'tinymce/js/tinymce/plugins/searchreplace/plugin.min.js';
import 'tinymce/js/tinymce/plugins/autolink/plugin.min.js';
import 'tinymce/js/tinymce/plugins/image/plugin.min.js';
import 'tinymce/js/tinymce/plugins/fullscreen/plugin.min.js';
import 'tinymce/js/tinymce/plugins/link/plugin.min.js';
import 'tinymce/js/tinymce/plugins/hr/plugin.min.js';
import 'tinymce/js/tinymce/plugins/media/plugin.min.js';
import 'tinymce/js/tinymce/plugins/pagebreak/plugin.min.js';
import 'tinymce/js/tinymce/plugins/nonbreaking/plugin.min.js';
import 'tinymce/js/tinymce/plugins/insertdatetime/plugin.min.js';
import 'tinymce/js/tinymce/plugins/anchor/plugin.min.js';
import 'tinymce/js/tinymce/plugins/lists/plugin.min.js';
import 'tinymce/js/tinymce/plugins/imagetools/plugin.min.js';
import 'tinymce/js/tinymce/plugins/contextmenu/plugin.min.js';

export const Article = (props) => {
  tinymce.init({
    selector: '#tiny',
    height: 500,
    theme: 'modern',
    plugins: 'searchreplace autolink fullscreen image link media hr pagebreak nonbreaking anchor insertdatetime lists imagetools contextmenu',
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
   });

  return (
    <div>
      <div class = "article-edit-area">
      <textarea id = "tiny">
        This is a TEST
      </textarea>
      </div>
      <div class = "bento-list-data" align = "center" ></div>
    </div>
  )
};
