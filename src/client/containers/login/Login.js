import React, { Component } from 'react';
import { InputBox } from '../../components/forms';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css';
import axios from 'axios';

class LoginForm extends Component {
  constructor(props){
    super(props);
    console.log('Show PROPS', this.props.data);
  }

   userHasAuthenticated(authData) {
    this.setState({ 
      isAuthenticated: authData.status,
      auth_token: authData.auth_token,
      uid: authData.uid,
      name: authData.name
    });
    localStorage.setItem(this.props.data.key, JSON.stringify(this.state));
    if (authData.status) window.location = '/dashboard';
  }
  

  
  /*
  componentDidMount() {
    this.hydrateStateWithLocalStorage();
    // add event listener to save state to localStorage
    // when user leaves/refreshes the page
    window.addEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );
  }

  componentWillUnmount() {
    window.removeEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );
    // saves if component has a chance to unmount
    this.saveStateToLocalStorage();
  }

  hydrateStateWithLocalStorage() {
    // for all items in state
    for (let key in this.state) {
      // if the key exists in localStorage
      if (localStorage.hasOwnProperty(key)) {
        // get the key's value from localStorage
        let value = localStorage.getItem(key);

        // parse the localStorage string and setState
        try {
          value = JSON.parse(value);
          this.setState({ [key]: value });
        } catch (e) {
          // handle empty string
          this.setState({ [key]: value });
        }
      }
    }
  }

  saveStateToLocalStorage() {
    // for every item in React state
    for (let key in this.state) {
      // save to localStorage
      localStorage.setItem(key, JSON.stringify(this.state[key]));
    }
  }

  updateInput(key, value) {
    // update react state
    this.setState({ [key]: value });
  }

  addItem() {
    // create a new item with unique id
    const newItem = {
      id: 1 + Math.random(),
      value: this.state.newItem.slice()
    };

    // copy current list of items
    const list = [...this.state.list];

    // add the new item to the list
    list.push(newItem);

    // update state with new list, reset the new item input
    this.setState({
      list,
      newItem: ""
    });
  }

  deleteItem(id) {
    // copy current list of items
    const list = [...this.state.list];
    // filter out the item being deleted
    const updatedList = list.filter(item => item.id !== id);

    this.setState({ list: updatedList });
  }

*/


  handleClick(event){
    let self = this;
    var apiBaseUrl = "http://localhost:4000/login";
    var payload = {
      "email": this.state.username,
      "password": this.state.password
    }

    axios.post(apiBaseUrl, payload)
    .then(function (response) {
      console.log('Status:', response.data);
      if (response.data.auth_token) {
        self.userHasAuthenticated({
          status: true,
          auth_token: response.data.auth_token,
          uid: response.data.uid,
          name: response.data.name
        });
      }
      
    })
    .catch(function (error) {
      console.log(error);
    });
  
  }

  render() {
    console.log('Set Status:', this.state);
    return (
      <div className = "BodyContainer">
        <div className="LoginForm">
          <div class="container">
            <div class="login-form">
              <div class="main-div">
                <div class="panel">
                  <h2>CMS Login | {process.env.ROCK_APP_DATABASE}</h2>
                  <p>Please enter your email and password</p>
                </div>
                <form id="Login">
                  <InputBox name = "email" class = "test" type = "email" id = "inputEmail" placeholder = "didarul.amin@nbcuni.com" onchangeHandler = {(event, newValue) => this.setState({username: event.target.value})}/>
                  <InputBox name = "password" type = "password" id = "inputPassword" placeholder = "Password"/>
                  <div class="forgot">
                    <a href="reset.html">Forgot password?</a>
                  </div>
                  <button type="button" class="btn btn-primary"  onClick={(event) => this.handleClick(event)}>Login</button>
                </form>
              </div>
              <p class="botto-text"> NBC News | Today | MSNBC </p>
              </div>
            </div>
        </div>
      </div>
    );
  }
}
export default LoginForm;