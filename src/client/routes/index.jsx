import Login from '../containers/login/Login';
import ContentEditFull from '../containers/article/ContentEditFull';
const indexRoutes = [{
      path: "/login",
      sidebarName: "Dashboard",
      navbarName: "Material Dashboard",
      component: Login
    },
    {
      path: "/dashboard",
      sidebarName: "User Profile",
      navbarName: "Profile",
      component: ContentEditFull
    },
    { redirect: true, path: "/", to: "/login", navbarName: "Redirect" 
  }];

export default indexRoutes;
