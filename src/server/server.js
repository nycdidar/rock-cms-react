const config = require('../config');
const express = require('express');
const mysql = require('mysql');
require('dotenv').config();

const app = express();

let bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Dynamic Routing
app.all('/:section/:node_id?/:action?', function (req, res, next) {
  console.log("Server Started");
  //console.log(process.env.DB_PASS);
  console.log(req.params.section);
  const rcmsRoute = require("./router.js");
  let route = new rcmsRoute();
  route.routeURL(req.params, res, req);
});



/*

function someCall(){
  return new Promise(resolve => {
    console.log('first call starts')
    setTimeout(()=>{
      console.log('first call end')
      resolve('first')
    }, 3000)
  })
}

function anotherCall(){
  return new Promise(resolve => {
    console.log('second call starts')
    setTimeout(()=>{
      console.log('second call end')
      resolve('second')
    }, 1000)
  })
}

async function main(){
  console.log('array', [await someCall(), await anotherCall()]);
  console.log('object', {
    first: await someCall(),
    second: await anotherCall()
  });
}


main();
*/



app.listen(config.server_port, () => console.log(`Listening on port ${config.server_port}`));
