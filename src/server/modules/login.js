/**
 * RCMS Login Module
 */
const crypto = require('crypto');
const logger = require('../../shared/logger');
const db = require('./db');

function showUserInfo (userData, role, res) {
  let authToken = crypto.randomBytes(20).toString('hex');

  if (typeof(userData) === 'undefined') {
    logger.error(`500 | bad_auth | User ${userData.mail} failed to login`);
    res.send({ status: 'failed', msg: 'Bad authentication. Wrong credential.' }); 
    return;  
  }

  if (userData.status !== 1) {
    logger.error(`500 | user_not_active | User ${userData.mail} failed to login`);
    res.send({ status: 'failed', msg: 'User not active.' });     
  } 

  db.query(`UPDATE users SET auth_token = '${authToken}' WHERE uid = '${userData.uid}'`);

  let roles = [];
  for (const [key, value] of Object.entries(role)) {
    roles.push(value.role);
  }

  let todayAccess = (userData.today === 1) ? 'true' : 'false';
  let msnbcAccess = (userData.msnbc === 1) ? 'true' : 'false';
  let nbcnewsAccess = (userData.nbcnews === 1) ? 'true' : 'false';
  let curationAccess = (userData.curation === 1) ? 'true' : 'false';
  //console.log("User Data: ", userData);
  logger.info(`200 | auth_successful | User ${userData.uid} logged in successfully`);
  res.send({ 
    status: 'ok', 
    auth_token: authToken,
    name: userData.name,
    uid: userData.uid,
    email: userData.mail,
    login: userData.login,
    access: userData.access,
    picture: userData.picture,
    roles: roles, 
    brands: { today: todayAccess, msnbc: msnbcAccess, nbcnews: nbcnewsAccess, curation: curationAccess },
    landing_page: userData.landingpage
  });
}

module.exports = function () {
  return {
    render: async function (urlParams, res, req) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      console.log('We are HERE !!!!', req.body);
      
      let userEmail = (req.body.email) ?  (req.body.email) : false;  
      
      if (!userEmail) {
        res.send({ status: 'failed', msg: 'Process Failed due to no email address found.' });   
        return; 
      }

      let getUserInfo = await db.fetchObj(`SELECT * FROM users WHERE mail = '${userEmail}'`);
      let getUserRolesQ = await db.query(`SELECT rl.name as role FROM users u LEFT JOIN users_roles r ON r.uid = u.uid LEFT JOIN role rl ON rl.rid = r.rid WHERE u.uid = '${getUserInfo.uid}'`);
      return await showUserInfo(getUserInfo, getUserRolesQ, res);
    },
  };
}
