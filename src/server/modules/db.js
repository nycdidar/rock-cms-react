/**
 * MySQL DB Wrapper
 */

const config = require('../../config');
var mysql = require('mysql');
var settings = {};
var dbCon;

module.exports = {
   query(query) {
    return new Promise(resolve => {
        settings = config;
        dbCon = mysql.createConnection(settings.db_settings);
        dbCon.connect(function(err) {
          if (err) throw err;
          dbCon.query(query, function (err, result) {
            if (err) throw err;
            resolve(result);
          });
        });
    });
  },

  fetchObj(query) {
    return new Promise(resolve => {
        settings = config;
        dbCon = mysql.createConnection(settings.db_settings);
        dbCon.connect(function(err) {
          if (err) throw err;
          dbCon.query(query, function (err, result) {
            console.log('DB Query:', query);
            if (err) throw err;
            let rowObj = {};
            Object.keys(result[0]).forEach(key => {
              rowObj[key] = result[0][key];
            });
            resolve(rowObj);
          });
        });
    });
  }


}