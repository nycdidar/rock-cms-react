// RockCMS Server Router
module.exports = function () {
  return {
    routeURL: (urlParams, res, req) => {            
      urlParams.section = (typeof urlParams.section !== 'undefined') ? urlParams.section : '';
      let routeTo = './modules/login.js';
      if (urlParams.section.match(/^(login)$/)) {
        routeTo = './modules/login.js';
      }
      console.log(`routeTo: ${routeTo}`);
      const ContentMain = require(routeTo);
      const contentType = new ContentMain();
      return contentType.render(urlParams, res, req);
    },
  };
}
